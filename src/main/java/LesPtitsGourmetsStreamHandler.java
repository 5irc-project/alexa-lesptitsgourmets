import Handler.*;
import com.amazon.ask.Skill;
import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;

public class LesPtitsGourmetsStreamHandler extends SkillStreamHandler {

    private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                        new RecipeHandler(),
                        new LaunchRequestHandler(),
                        new NextStepHandler(),
                        new PreviousStepHandler(),
                        new StopHandler()
                )
                .build();
    }

    public LesPtitsGourmetsStreamHandler() {
        super(getSkill());
    }
}