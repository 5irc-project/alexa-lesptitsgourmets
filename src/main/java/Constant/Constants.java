package Constant;

public class Constants {
    public static final String URL_API = "https://les-ptits-gourmets.herokuapp.com/";
    public static final String RECIPE_NAME_SLOT = "NameRecipe";
    public static final String RECIPE_NAME_KEY = "recipeName";
    public static final String RECIPE_STEP_ID = "recipeStepId";
}
